package io.hodl.blockstream

import io.hodl.utils.TestUtils.service
import kotlinx.coroutines.runBlocking
import org.bitcoinj.core.Sha256Hash
import org.junit.Assert

import org.junit.Test

class BlockstreamServiceTest {

    @Test
    fun getUTXO() = runBlocking {
        val info = service.getUTXO("2NC9DZ5eGurR24EXEgHouVSUzuPfZ7yVwrv")
    }

    @Test
    fun getTxs() = runBlocking {
        val txs = service.getTxs("2NC9DZ5eGurR24EXEgHouVSUzuPfZ7yVwrv")
        Assert.assertTrue(txs.isEmpty().not())
    }

    @Test
    fun getAddresInfo() = runBlocking {
        val info = service.getAddresInfo("2NC9DZ5eGurR24EXEgHouVSUzuPfZ7yVwrv")
        Assert.assertEquals(info.address, "2NC9DZ5eGurR24EXEgHouVSUzuPfZ7yVwrv")
    }

    @Test
    fun getHash() {
        Sha256Hash.wrap("0000000000000002582bb480ee90723ad2783b0a75fd9d024d18040fc9aa76b7")
    }
}