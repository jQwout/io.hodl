package io.hodl.utils

import io.hodl.ktor.BlockstreamServiceKtor
import io.hodl.ktor.HttpClientFactory
import org.bitcoinj.core.Base58
import org.bitcoinj.core.ECKey
import org.bitcoinj.core.LegacyAddress
import org.bitcoinj.core.NetworkParameters
import org.bitcoinj.params.TestNet3Params
import org.bitcoinj.script.Script
import org.bitcoinj.store.MemoryFullPrunedBlockStore
import org.bitcoinj.wallet.Wallet
import java.io.File

object TestUtils {

    val networkParameters = NetworkParameters.fromID(NetworkParameters.ID_TESTNET)!!

    val memoryBlockChain = MemoryFullPrunedBlockStore(networkParameters, 1)

    val blockIoTestAddress = LegacyAddress.fromString(networkParameters,"2NC9DZ5eGurR24EXEgHouVSUzuPfZ7yVwrv")

    //mysDhLVPQY541ZmASUiaQxCkz8W7WjyCdm
    val key = ECKey.fromPrivate(
        Base58.decodeToBigInteger(
            "153Kxzh5ASDwRQxEDX4sKwiqB7xCLuXb6GACZtefAtb5q"
        )
    )

    val address = LegacyAddress.fromKey(TestNet3Params.get(), key, Script.ScriptType.P2PKH)

    val service = BlockstreamServiceKtor(
        HttpClientFactory.create("https://blockstream.info/testnet/",null)
    )

    //mnaZJhF4eGXEt86BR7D8SQmvaFKExu7vy2
    fun loadWalletFromFile(): Wallet {
        return Wallet.loadFromFile(File("WalletAppKit.wallet"))
    }

    fun loadWalletFromKey2(): Wallet {
        val w = Wallet.createBasic(networkParameters).apply {
            importKey(key)
        }
        return w
    }

    fun getStrFromFile(): String {
        val classLoader = javaClass.classLoader
        val file = File(classLoader.getResource("txs_sample.json").file)
        return file.readText()
    }
}