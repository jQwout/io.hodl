package io.hodl.crypto

import io.hodl.utils.TestUtils
import io.hodl.utils.TestUtils.key
import org.bitcoinj.core.*
import org.bitcoinj.params.TestNet3Params
import org.bitcoinj.script.Script
import org.bitcoinj.wallet.Wallet
import org.junit.Test
import java.io.File

class KeyTest {

    @Test
    fun restoreAddressByKey() {
        val key = ECKey.fromPrivate(
            Base58.decodeToBigInteger(
                "A6zshBjSZmZjGTqkRB1ucqPssx75TxkSWqnuKLcTJXvR"
            )
        )
        val address = Address.fromKey(TestNet3Params.get(), key, Script.ScriptType.P2PKH)

        println(address.toString())

    }

    @Test
    fun loadWallet() {
        val wallet = Wallet.loadFromFile(File("WalletAppKit.wallet"))
        val rq = wallet.createSend(TestUtils.blockIoTestAddress, Coin.valueOf(700))
        println(rq.toHexString())
    }


    private fun loadFromDumpKey() {
        val address = Address.fromKey(
            TestNet3Params.get(),
            key,
            Script.ScriptType.P2PKH
        )

        println(address)
    }
}