package io.hodl.crypto

import io.hodl.utils.TestUtils
import org.apache.log4j.Appender
import org.apache.log4j.PropertyConfigurator
import org.bitcoinj.core.Coin
import org.bitcoinj.core.LegacyAddress
import org.bitcoinj.kits.WalletAppKit
import org.bitcoinj.wallet.Wallet
import org.junit.Test
import java.io.File

class WApTests {

    @Test
    fun test() {
        val wak = WalletAppKit(TestUtils.networkParameters, File("."), "WalletAppKit")
        try {
            wak.startAsync()
            wak.awaitRunning()
            wak.setBlockingStartup(false)
            wak.peerGroup().minBroadcastConnections = 4

            println(wak.wallet().balance)
            val q = wak.wallet().createSend(TestUtils.address, Coin.valueOf(100))
            println("HEX: " +q.toHexString())
        } catch (e: Throwable) {
            e.printStackTrace()
            println("throw :${e}")
            println("throw :${e.localizedMessage}")
        } finally {
            wak.awaitTerminated()
            wak.stopAsync()
        }
    }

}