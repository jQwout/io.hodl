package io.hodl.crypto

import io.hodl.ktor.MapperUtils
import io.hodl.utils.TestUtils
import kotlinx.coroutines.*
import org.bitcoinj.core.*
import org.bitcoinj.params.TestNet3Params
import org.bitcoinj.script.Script
import org.bitcoinj.script.ScriptBuilder
import org.bitcoinj.wallet.SendRequest
import org.bouncycastle.util.encoders.Hex
import org.junit.Test

class TransactionFactoryTest {

    val factory = BitcoinjTransactionFactory(
        networkParameters = TestUtils.networkParameters,
        address = TestUtils.address,
        key = TestUtils.key,
    )

    @Test
    fun loadUtxoAndBuild() = runBlocking {
        Context.getOrCreate(TestNet3Params.get())
        val list = TestUtils.service.getUTXO(TestUtils.address.toString())
            .map { MapperUtils.mapUtxo(it) }
        val tx = factory.createRawTransaction(list, TestUtils.blockIoTestAddress.toString(), 0.0005.toBigDecimal()).first
        println(tx)
        tx.inputs.forEachIndexed { index, transactionInput ->
            transactionInput.scriptSig.correctlySpends(
                tx,
                index,
                null,
                null,
                ScriptBuilder.createOutputScript(TestUtils.address),
                setOf(Script.VerifyFlag.DERSIG)
            )
        }
        val rq = SendRequest.forTx(tx)
        println(rq.tx.toHexString())
    }
}