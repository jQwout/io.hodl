package io.hodl.crypto

import io.hodl.domain.TransactionCostHelper
import io.hodl.domain.pojo.UtxoDto
import io.hodl.domain.pojo.summary
import io.hodl.utils.TestUtils
import kotlinx.coroutines.runBlocking
import org.bitcoinj.core.Coin
import org.bitcoinj.core.Sha256Hash
import org.junit.Test
import java.util.*

class ChargeTest {

    val factory = BitcoinjTransactionFactory(
        networkParameters = TestUtils.networkParameters,
        address = TestUtils.address,
        key = TestUtils.key,
    )
    val addressStr = "2NC9DZ5eGurR24EXEgHouVSUzuPfZ7yVwrv"

    @Test
    fun test() {
        runBlocking {
            val send = Coin.ofSat(1000)
            val utxo = listOf(createUtxo(2000))
            buildTest(send, utxo)
        }
    }

    @Test
    fun test2() {
        runBlocking {
            val send = Coin.ofSat(3400)
            val utxo = listOf(
                createUtxo(2000),
                createUtxo(2000),
                createUtxo(2000),
                createUtxo(2000),
                createUtxo(2000),
            )
            buildTest(send, utxo)
        }
    }

    private fun buildTest(send: Coin, utxo: List<UtxoDto>) {
        val (tx, scoredFee) = factory.createRawTransaction(
            utxo,
            addressStr,
            send.toBtc(),
        )

        val exFee = TransactionCostHelper.scoreFee(tx.vsize)
        val factFee = utxo.summary() - tx.outputSum.value - send.value
        println("tx.size: " + tx.vsize)
        println("tx.fee: " + tx.fee)
        println(
            "final:" +
                    "balance:${utxo.summary()}\n" +
                    "send:$send\n" +
                    "outs:${tx.outputSum.value}\n" +
                    "txSize: ${tx.messageSize}\n" +
                    "fee cost: $factFee\n" +
                    "exfee cost: $exFee\n"
        )
    }


    private fun createUtxo(satoshi: Long) =
        UtxoDto(UUID.randomUUID().toString(), satoshi, 0, Sha256Hash.ZERO_HASH.toString(), true)
}