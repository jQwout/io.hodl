package io.hodl.domain

import io.hodl.domain.pojo.TransactionHistoryItem

interface TransactionHistoryRepository {

    suspend fun getTransactions(address: String): List<TransactionHistoryItem>

}