package io.hodl.domain

import io.hodl.domain.pojo.BitcoinTransaction
import io.hodl.domain.pojo.UtxoDto
import java.math.BigDecimal

interface BitcoinTransactionFactory {

     fun createTransaction(utxos: List<UtxoDto>, to: String, amount: BigDecimal): BitcoinTransaction
}