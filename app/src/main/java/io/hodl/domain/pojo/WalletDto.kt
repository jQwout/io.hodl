package io.hodl.domain.pojo

import java.math.BigDecimal

class WalletDto(
    val address: String,
    val balanceAvailable: BigDecimal,
)