package io.hodl.domain.pojo

import java.math.BigDecimal
import java.util.*

data class TransactionHistoryItem(
    val txId: String,
    val address: String,
    val amount: BigDecimal,
    val isSend: Boolean,
    val time: Date?,
) {
    val isConfirmed : Boolean = time != null
}
