package io.hodl.domain.pojo

import java.math.BigDecimal

class BitcoinTransaction(
    val txId: String,
    val hex: String,
    val fee: BigDecimal
) : java.io.Serializable