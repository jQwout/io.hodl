package io.hodl.domain.pojo

data class UtxoDto(
    val txid: String,
    val value: Long = 0,
    val vout: Int,
    val blockHash: String? = null,
    val confirmed: Boolean
)

fun List<UtxoDto>.summary() = fold(0L) { a, b -> a + b.value }