package io.hodl.domain

import io.hodl.domain.TransactionsUseCase.Companion.MINIMUM_FEE_AMOUNT
import java.math.BigDecimal

object TransactionCostHelper {

    fun scoreFee(
        txSize: Int,
        feePerByte: Double = MINIMUM_FEE_AMOUNT
    ): BigDecimal {
        val fee = (txSize * feePerByte)
        return maxOf(fee, MINIMUM_FEE_AMOUNT).toBigDecimal()
    }

    fun scoreChange(
        balance: BigDecimal,
        amount: BigDecimal,
        fee: BigDecimal
    ): BigDecimal {
        return balance - amount - fee
    }
}