package io.hodl.domain

import io.hodl.domain.pojo.UtxoDto

interface UtxoRepository {

    suspend fun getUtxo(force: Boolean): List<UtxoDto>
}