package io.hodl.domain

import io.hodl.domain.pojo.BitcoinTransaction
import java.math.BigDecimal

interface TransactionsUseCase {
    suspend fun buildTransactionFromUtxos(to: String, amount: BigDecimal): BitcoinTransaction

    suspend fun sendRawTransaction(tx: BitcoinTransaction):  Result<String>

    companion object {
        const val MINIMUM_FEE_AMOUNT = 0.000001
    }
}