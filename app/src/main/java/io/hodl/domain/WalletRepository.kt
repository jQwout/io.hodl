package io.hodl.domain

import io.hodl.blockstream.BlockstreamService
import io.hodl.domain.pojo.WalletDto
import org.bitcoinj.core.Coin

interface WalletRepository {

    suspend fun getWallet(force: Boolean): WalletDto

}