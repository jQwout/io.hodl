package io.hodl

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import io.hodl.di.HodlComponentProvider
import io.hodl.di.HodlTransactionComponent
import io.hodl.di.HodlTransactionComponentBuilder
import io.hodl.blockstream.BlockstreamService.Companion.BASE_URL
import io.hodl.ktor.HttpClientFactory
import org.bitcoinj.core.NetworkParameters
import org.bitcoinj.params.TestNet3Params
import org.bitcoinj.wallet.Wallet

class HodlApp : Application(), HodlComponentProvider {

    private lateinit var _transactionComponent: HodlTransactionComponent
    override val transactionComponent: HodlTransactionComponent
        get() = _transactionComponent

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        _transactionComponent = HodlTransactionComponentBuilder.build(
            TestNet3Params.get(),
            HttpClientFactory.create(BASE_URL)
        )
    }
}