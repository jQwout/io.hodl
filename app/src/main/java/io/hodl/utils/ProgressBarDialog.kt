package io.hodl.utils

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import io.hodl.R

class ProgressBarDialog : DialogFragment(R.layout.progress_dialog_fragment) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.ProgressDialog)
        isCancelable = false
    }
}

class ErrorDialog : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, theme)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(requireContext())
            .setMessage(getString(R.string.error_dialog_title_text))
            .setPositiveButton(getString(R.string.error_dialog_ok)) { _, _ ->
                dismissNow()
            }
            .create()
    }

    override fun onCancel(dialog: DialogInterface) {
        (activity as? OnErrorDialogCancel)?.onErrorDialogCancel()
        super.onCancel(dialog)
    }

    override fun onDismiss(dialog: DialogInterface) {
        (activity as? OnErrorDialogCancel)?.onErrorDialogCancel()
        super.onDismiss(dialog)
    }

}

interface OnErrorDialogCancel {
    fun onErrorDialogCancel()
}