package io.hodl.utils

import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*

object StringViewUtils {


    fun txStrHumanReadable(txId: String) = txId.take(4) +"..." + txId.takeLast(4)

    fun formatBtc(bigDecimal: BigDecimal) = String.format("%,.10f", bigDecimal)

    fun formatDate(date: Date): String {
        return SimpleDateFormat("MM/dd/yyyy 'at' hh:mm", Locale.getDefault()).format(date)
    }
 }