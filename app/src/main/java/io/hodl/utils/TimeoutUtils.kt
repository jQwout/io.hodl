package io.hodl.utils

object TimeoutUtils {

    const val NON_FLASH_TIMEOUT= 400L

    const val WALLET_POLLING_TIMEOUT = 12_000L
}