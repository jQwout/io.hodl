package io.hodl.blockstream

import io.hodl.domain.TransactionHistoryRepository
import io.hodl.blockstream.pojo.Vin
import io.hodl.blockstream.pojo.Vout
import io.hodl.domain.pojo.TransactionHistoryItem
import io.hodl.ktor.MapperUtils
import org.bitcoinj.core.Coin
import java.math.BigDecimal

class BlockStreamTransactionHistoryRepository(private val service: BlockstreamService) : TransactionHistoryRepository {
        override suspend fun getTransactions(address: String): List<TransactionHistoryItem> {
            return service.getTxs(address).map {
                val isSend = checkSend(it.vin, address)
                TransactionHistoryItem(
                    it.txid,
                    address,
                    scoreAmount(it.vout, address, it.fee, isSend),
                    isSend,
                    it.status.blockTime
                )
            }
        }

        private fun scoreAmount(
            outs: List<Vout>,
            address: String,
            fee: Long,
            isSend: Boolean,
        ): BigDecimal {

            val value = if (isSend) {
                outs.filter { it.address != address }.foldRight(0L) { n, a -> n.value + a } + fee
            } else {
                outs.first { it.address == address }.value
            }

            return MapperUtils.ofSatToBtc(value)
        }

        private fun checkSend(vins: List<Vin>, fromAddress: String): Boolean {
            return vins.any { it.prevout.address == fromAddress }
        }
    }