package io.hodl.blockstream

import io.hodl.domain.BitcoinTransactionFactory
import io.hodl.domain.TransactionsUseCase
import io.hodl.domain.pojo.BitcoinTransaction
import io.hodl.ktor.MapperUtils
import java.math.BigDecimal

class BlockStreamTransactionsUseCase(
    private val transactionFactory: BitcoinTransactionFactory,
    private val blockstreamService: BlockstreamService,
    private val sourceAddress: String
) : TransactionsUseCase {

    override suspend fun buildTransactionFromUtxos(to: String, amount: BigDecimal): BitcoinTransaction {
        val list = blockstreamService.getUTXO(sourceAddress, false).map { MapperUtils.mapUtxo(it) }
        return transactionFactory.createTransaction(list, to, amount)
    }

    override suspend fun sendRawTransaction(tx: BitcoinTransaction): Result<String> {
        return kotlin.runCatching { blockstreamService.sendRawTx(tx) }
    }

}