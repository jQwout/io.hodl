package io.hodl.blockstream

import io.hodl.domain.UtxoRepository
import io.hodl.domain.pojo.UtxoDto
import io.hodl.ktor.MapperUtils

class BlockStreamUtxoRepository(
    private val blockstreamService: BlockstreamService,
    private val addressSource: String
) : UtxoRepository {
    override suspend fun getUtxo(force: Boolean): List<UtxoDto> =
        blockstreamService.getUTXO(addressSource, force).map { MapperUtils.mapUtxo(it) }
}