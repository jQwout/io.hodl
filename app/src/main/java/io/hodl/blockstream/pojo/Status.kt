package io.hodl.blockstream.pojo


import io.hodl.ktor.HttpClientFactory
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.util.Date

@Serializable
data class Status(
    @SerialName("block_hash")
    val blockHash: String? = null,
    @SerialName("block_height")
    val blockHeight: Int? = null,
    @SerialName("block_time")
    @Serializable(with = HttpClientFactory.BlockTimeToDate::class)
    val blockTime: Date? = null,
    @SerialName("confirmed")
    val confirmed: Boolean
)