package io.hodl.blockstream.pojo


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Vin(
    @SerialName("is_coinbase")
    val isCoinbase: Boolean,
    @SerialName("prevout")
    val prevout: Vout,
    @SerialName("txid")
    val txid: String,
    @SerialName("vout")
    val vout: Long,
)