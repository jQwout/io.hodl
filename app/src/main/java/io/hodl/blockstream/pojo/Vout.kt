package io.hodl.blockstream.pojo


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Vout(
    @SerialName("value")
    val value: Long,
    @SerialName("scriptpubkey_address")
    val address: String,
)