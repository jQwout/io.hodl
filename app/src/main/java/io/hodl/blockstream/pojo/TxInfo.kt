package io.hodl.blockstream.pojo


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TxInfo(
    @SerialName("fee")
    val fee: Long,
    @SerialName("locktime")
    val locktime: Long,
    @SerialName("size")
    val size: Long,
    @SerialName("status")
    val status: Status,
    @SerialName("txid")
    val txid: String,
    @SerialName("version")
    val version: Int,
    @SerialName("vin")
    val vin: List<Vin>,
    @SerialName("vout")
    val vout: List<Vout>,
    @SerialName("weight")
    val weight: Long
)