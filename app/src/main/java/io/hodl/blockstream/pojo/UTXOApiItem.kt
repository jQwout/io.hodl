package io.hodl.blockstream.pojo


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class UTXOApiItem(
    @SerialName("status")
    val status: Status,
    @SerialName("txid")
    val txid: String,
    @SerialName("value")
    val value: Long,
    @SerialName("vout")
    val vout: Int
)