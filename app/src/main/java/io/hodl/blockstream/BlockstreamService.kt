package io.hodl.blockstream

import io.hodl.blockstream.pojo.AddressInfo
import io.hodl.blockstream.pojo.TxInfo
import io.hodl.blockstream.pojo.UTXOApiItem
import io.hodl.domain.pojo.BitcoinTransaction

interface BlockstreamService {
    suspend fun getUTXO(address: String, force: Boolean = false): List<UTXOApiItem>

    suspend fun getTxs(address: String): List<TxInfo>

    suspend fun getAddresInfo(address: String): AddressInfo

    suspend fun sendRawTx(bitcoinTransaction: BitcoinTransaction): String

    companion object {
        const val BASE_URL = "https://blockstream.info/testnet/api"
    }
}