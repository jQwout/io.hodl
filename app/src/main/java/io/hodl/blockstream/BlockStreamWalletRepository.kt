package io.hodl.blockstream

import io.hodl.domain.WalletRepository
import io.hodl.domain.pojo.WalletDto
import io.hodl.domain.pojo.summary
import io.hodl.ktor.MapperUtils
import org.bitcoinj.core.Coin

class BlockStreamWalletRepository(
    private val address: String,
    private val service: BlockstreamService
) : WalletRepository {
    override suspend fun getWallet(force: Boolean): WalletDto {
        val list = service.getUTXO(address, force).map { MapperUtils.mapUtxo(it) }
        val balance = MapperUtils.ofSatToBtc(list.summary())
        return WalletDto(
            address,
            balance
        )
    }
}