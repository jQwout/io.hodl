package io.hodl.di

import io.hodl.blockstream.BlockStreamTransactionHistoryRepository
import io.hodl.blockstream.BlockStreamTransactionsUseCase
import io.hodl.blockstream.BlockStreamUtxoRepository
import io.hodl.blockstream.BlockStreamWalletRepository
import io.hodl.crypto.BitcoinjTransactionFactory
import io.hodl.crypto.KeyProvider
import io.hodl.domain.*
import io.hodl.ktor.BlockstreamServiceKtor
import io.hodl.ktor.HttpClientFactory
import io.ktor.client.*
import org.bitcoinj.core.NetworkParameters
import org.bitcoinj.params.TestNet3Params

class HodlTransactionComponent(
    val transactionFactory: BitcoinTransactionFactory,
    val transactionsUseCase: TransactionsUseCase,
    val transactionHistoryRepository: TransactionHistoryRepository,
    val walletRepository: WalletRepository,
    val utxoRepository: UtxoRepository,
)

object HodlTransactionComponentBuilder {

    fun build(
        networkParameters: NetworkParameters,
        httpClient: HttpClient,
    ): HodlTransactionComponent {
        val bitcoinJDeps = BitcoinJDeps(networkParameters)
        val blockStreamDeps = BlockStreamDeps(httpClient, bitcoinJDeps.bitcoinjTransactionFactory, bitcoinJDeps.addressStr)
        return build(bitcoinJDeps, blockStreamDeps)
    }

    private fun build(
        bitcoinJDeps: BitcoinJDeps,
        blockStreamDeps: BlockStreamDeps
    ): HodlTransactionComponent {
        return HodlTransactionComponent(
            transactionFactory = bitcoinJDeps.bitcoinjTransactionFactory,
            transactionsUseCase = blockStreamDeps.transactionsUseCase,
            transactionHistoryRepository = blockStreamDeps.transactionHistoryRepository,
            walletRepository = blockStreamDeps.walletRepository,
            utxoRepository = blockStreamDeps.utxoRepository
        )
    }
}

class BitcoinJDeps(val networkParameters: NetworkParameters) {
    val key = KeyProvider.getPrivateKey()
    val address = KeyProvider.getAddressFromKey(
        key,
        networkParameters
    )
    val addressStr = address.toString()
    val bitcoinjTransactionFactory = BitcoinjTransactionFactory(networkParameters, address, key)
}

class BlockStreamDeps(
    httpClient: HttpClient,
    transactionFactory: BitcoinTransactionFactory,
    address: String
) {
    private val blockstreamService = BlockstreamServiceKtor(httpClient)
    val transactionsUseCase = BlockStreamTransactionsUseCase(transactionFactory, blockstreamService, address)
    val transactionHistoryRepository = BlockStreamTransactionHistoryRepository(blockstreamService)
    val walletRepository = BlockStreamWalletRepository(address, blockstreamService)
    val utxoRepository = BlockStreamUtxoRepository(blockstreamService, address)
}