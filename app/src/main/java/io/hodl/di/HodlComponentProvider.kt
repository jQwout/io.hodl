package io.hodl.di

interface HodlComponentProvider {
    val transactionComponent: HodlTransactionComponent
}