package io.hodl.crypto

import io.hodl.BuildConfig
import org.bitcoinj.core.*
import org.bitcoinj.params.TestNet3Params
import org.bitcoinj.script.Script

object KeyProvider {

    fun getPrivateKey(): ECKey {
        return ECKey.fromPrivate(
            Base58.decodeToBigInteger(
                BuildConfig.PRIVATE_KEY // 153Kxzh5ASDwRQxEDX4sKwiqB7xCLuXb6GACZtefAtb5q
            ),
            true
        )
    }

    fun getAddressFromKey(
        ecKey: ECKey,
        networkParameters: NetworkParameters = TestNet3Params.get(),
        script: Script.ScriptType = Script.ScriptType.P2PKH
    ): Address {
        return LegacyAddress.fromKey(networkParameters, ecKey, script)
    }

}