package io.hodl.crypto

import io.hodl.domain.BitcoinTransactionFactory
import io.hodl.domain.TransactionCostHelper.scoreChange
import io.hodl.domain.TransactionCostHelper.scoreFee
import io.hodl.domain.pojo.BitcoinTransaction
import io.hodl.domain.pojo.UtxoDto
import io.hodl.domain.pojo.summary
import org.bitcoinj.core.*
import org.bitcoinj.crypto.TransactionSignature
import org.bitcoinj.script.ScriptBuilder
import org.bitcoinj.script.ScriptError
import org.bitcoinj.script.ScriptException
import org.bitcoinj.script.ScriptPattern
import java.math.BigDecimal
import java.math.RoundingMode

class BitcoinjTransactionFactory(
    private val networkParameters: NetworkParameters,
    private val address: Address,
    private val key: ECKey,
) : BitcoinTransactionFactory {

    override fun createTransaction(utxos: List<UtxoDto>, to: String, amount: BigDecimal): BitcoinTransaction {
        val (tx, fee) = createRawTransaction(utxos, to, amount)
        return BitcoinTransaction(
            tx.txId.toString(),
            tx.toHexString(),
            fee
        )
    }

    fun createRawTransaction(utxos: List<UtxoDto>, to: String, amount: BigDecimal): Pair<Transaction, BigDecimal> {
        val balance = Coin.ofSat(utxos.summary()).toBtc()
        return Transaction(networkParameters).let {
            it.addOutput(Coin.ofBtc(amount), Address.fromString(networkParameters, to))
            it.addOutput(Coin.ofSat(-1), address)
            it.collectInputs(utxos)
            val fee = scoreFee(it.vsize)
            val change = Coin.ofBtc(scoreChange(balance, amount, fee).setScale(10, RoundingMode.HALF_DOWN))
            //if (change.isNegative) throw InsufficientMoneyException(change.multiply(-1))
            it.outputs.last().value = change
            it.inputs.forEachIndexed { index, transactionOutput ->
                it.signManuality(index.toLong())
            }
            it to fee
        }
    }

    fun Transaction.collectInputs(list: List<UtxoDto>) {
        for (item in list) {
            if (item.blockHash != null) {
                addSignedInput(
                    createOutputPoint(item.vout.toLong(), item.txid),
                    ScriptBuilder.createOutputScript(address),
                    Coin.ofSat(item.value),
                    key
                )
            }
        }
    }

    private fun createOutputPoint(index: Long, shaHex: String): TransactionOutPoint {
        return TransactionOutPoint(
            networkParameters, index, Sha256Hash.wrap(shaHex)
        )
    }

    private fun Transaction.signManuality(i: Long) {
        val outScrpt = ScriptBuilder.createOutputScript(address)
        val hash = hashForSignature(i.toInt(), outScrpt, Transaction.SigHash.ALL, false)
        val ecdsaSignature = key.sign(hash)
        val txSignature = TransactionSignature(ecdsaSignature, Transaction.SigHash.ALL, false)
        if (ScriptPattern.isP2PK(outScrpt)) {
            getInput(i).scriptSig = ScriptBuilder.createInputScript(txSignature);
        } else {
            if (!ScriptPattern.isP2PKH(outScrpt)) {
                throw ScriptException(
                    ScriptError.SCRIPT_ERR_UNKNOWN_ERROR,
                    "Unable to sign this scrptPubKey: $outScrpt"
                );
            }
            getInput(i).scriptSig = ScriptBuilder.createInputScript(txSignature, key)
        }
    }
}