package io.hodl.ktor

import io.hodl.blockstream.pojo.UTXOApiItem
import io.hodl.domain.pojo.UtxoDto
import org.bitcoinj.core.Coin

object MapperUtils {

    fun mapUtxo(item: UTXOApiItem) = UtxoDto(
        item.txid,
        item.value,
        item.vout,
        item.status.blockHash,
        item.status.confirmed
    )


    fun ofSatToBtc(sat: Long) = Coin.ofSat(sat).toBtc()
}