package io.hodl.ktor

import io.hodl.blockstream.BlockstreamService
import io.hodl.blockstream.pojo.AddressInfo
import io.hodl.blockstream.pojo.TxInfo
import io.hodl.blockstream.pojo.UTXOApiItem
import io.hodl.domain.pojo.BitcoinTransaction
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*

class BlockstreamServiceKtor(
    private val client: HttpClient
) : BlockstreamService {

    override suspend fun getUTXO(address: String, force: Boolean): List<UTXOApiItem> = client
        .get("api/address/$address/utxo") {
            withCacheOrNot(force)
            contentType(ContentType.Application.Json)
        }
        .body()

    override suspend fun getTxs(address: String): List<TxInfo> = client
        .get("api/address/$address/txs") {
            contentType(ContentType.Application.Json)
        }
        .body()

    override suspend fun getAddresInfo(address: String): AddressInfo = client
        .get("api/address/$address") {
            contentType(ContentType.Application.Json)
        }
        .body()

    override suspend fun sendRawTx(bitcoinTransaction: BitcoinTransaction): String {
        return client.post("api/tx") {
            setBody(bitcoinTransaction.hex)
            contentType(ContentType.Text.Plain)
        }.body()
    }

    private fun HttpRequestBuilder.withCacheOrNot(force: Boolean) {
        if (force) {
            headers.append("Cache-Control", "no-cache")
        } else {
            headers.append("Cache-Control", "max-age=30")
        }
    }
}