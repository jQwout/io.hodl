package io.hodl.ktor

import android.util.Log
import io.ktor.client.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.cache.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.util.*
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.Json
import java.util.*

object HttpClientFactory {

    fun create(baseUrlStr: String, networkLogger: Logger? = hodlLogger) = HttpClient {
        expectSuccess = true
        defaultRequest {
            url(baseUrlStr)
        }
        install(HttpCache)
        install(Logging) {
            networkLogger?.let { logger = it }
        }
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
                prettyPrint = true
                isLenient = true
            })
        }
        install(HttpTimeout) {
            requestTimeoutMillis = 30_000
            connectTimeoutMillis = 30_000
        }
    }

    val hodlLogger = object : Logger {
        override fun log(message: String) {
            Log.d("=== hold network log", message)
        }
    }

    class BlockTimeToDate : KSerializer<Date?> {
        private val ms = 1000

        override val descriptor: SerialDescriptor =
            PrimitiveSerialDescriptor("Date", PrimitiveKind.LONG)

        override fun deserialize(decoder: Decoder): Date {
            return Date(
                decoder.decodeLong() * ms
            )
        }

        override fun serialize(encoder: Encoder, value: Date?) {
            if (value == null)
                encoder.encodeNull()
            else
                encoder.encodeLong(value.time / ms)
        }
    }
}