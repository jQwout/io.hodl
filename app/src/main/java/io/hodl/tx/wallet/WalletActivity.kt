package io.hodl.tx.wallet

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.*
import io.hodl.R
import io.hodl.databinding.WalletActivityBinding
import io.hodl.tx.fee.FeeViewComponent
import io.hodl.tx.history.TxHistoryBottomSheetDialog
import io.hodl.tx.info.TxInfoBottomSheetDialog
import io.hodl.utils.ErrorDialog
import io.hodl.utils.OnErrorDialogCancel
import io.hodl.utils.ProgressBarDialog
import io.hodl.utils.StringViewUtils
import io.hodl.utils.TimeoutUtils.NON_FLASH_TIMEOUT
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch

class WalletActivity : AppCompatActivity(), OnErrorDialogCancel {

    private val viewModel: WalletViewModel by viewModels { WalletViewModel.factory() }
    private val binding: WalletActivityBinding by lazy(LazyThreadSafetyMode.NONE) {
        WalletActivityBinding.inflate(layoutInflater)
    }
    private val addressInputContainer by lazy(LazyThreadSafetyMode.NONE) { binding.walletAddressToSendLayout }
    private val amountInputContainer by lazy(LazyThreadSafetyMode.NONE) { binding.walletAmountToSendLayout }
    private val feeComponent: FeeViewComponent by lazy(LazyThreadSafetyMode.NONE) {
        FeeViewComponent(binding.walletActivityFee)
    }
    private val progressBarDialog = ProgressBarDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        updateBalance()
        updateAddress()
        updateValidation()
        updateAddressValidation()
        updateFee()
        updateSendEnabled()
        updateHistoryButtonVisible()
        navigateOnTxInfo()
        navigateOnHistory()
        handleCommonErrors()
        updateDataLoading()
        navigateOnWebInfo()

        binding.walletAmountToSendEditText.doOnTextChanged { text, _, _, _ ->
            viewModel.inputAmount(text)
        }

        binding.walletAddressToSendEditText.doOnTextChanged { text, _, _, _ ->
            viewModel.inputAddress(text)
        }

        binding.walletActivitySendButton.setOnClickListener {
            viewModel.onSendClicked()
        }

        binding.walletReceiveAddress.setOnClickListener {
            val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("address", binding.walletReceiveAddress.text)
            clipboard.setPrimaryClip(clip)
        }

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        viewModel.needReloadScreen()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.wallet_menu, menu)
        menu.findItem(R.id.menu_action_history).isVisible = viewModel.historyList.value.isNullOrEmpty().not()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_action_history -> {
                viewModel.onHistoryClicked()
                true
            }
            else -> false
        }
    }

    override fun onErrorDialogCancel() {
        viewModel.needReloadScreen()
    }

    private fun updateBalance() = lifecycleScope.launch {
        viewModel.balance.collect { binding.walletAvailableAmount.text = it }
    }

    private fun updateValidation() = lifecycleScope.launch {
        viewModel.amountValidationError.collect {
            amountInputContainer.error = it?.let { getString(it.strRes) }
        }
    }

    private fun updateAddress() = lifecycleScope.launch {
        viewModel.walletAddress.collect {
            binding.walletReceiveAddress.text = it
            binding.walletReceiveAddress.isVisible = true
        }
    }

    private fun updateAddressValidation() = lifecycleScope.launch {
        viewModel.addressValidationError.collect {
            addressInputContainer.error = it?.let { getString(it.strRes) }
        }
    }

    private fun updateFee() = lifecycleScope.launch {
        viewModel.feeFlow.collectLatest {
            feeComponent.bind(it)
        }
    }

    private fun updateDataLoading() = lifecycleScope.launch {
        lifecycle.repeatOnLifecycle(Lifecycle.State.RESUMED) {
            viewModel.isDataLoading.debounce(NON_FLASH_TIMEOUT).collectLatest { isLoading ->
                if (isLoading) {
                    progressBarDialog.show(supportFragmentManager, null)
                } else {
                    if (progressBarDialog.isAdded) progressBarDialog.dismiss()
                }
            }
        }
    }

    private fun updateHistoryButtonVisible() = lifecycleScope.launch {
        viewModel.historyButtonEnable.collectLatest { hasHistory ->
            invalidateOptionsMenu()
        }
    }

    private fun updateSendEnabled() = lifecycleScope.launch {
        viewModel.sendEnabled.collectLatest {
            binding.walletActivitySendButton.isEnabled = it
        }
    }

    private fun navigateOnTxInfo() = lifecycleScope.launch {
        viewModel.onTxSuccess.collectLatest {
            TxInfoBottomSheetDialog.create(txId = it, txStr = StringViewUtils.txStrHumanReadable(it))
                .show(supportFragmentManager, null)
        }
    }

    private fun navigateOnWebInfo() = lifecycleScope.launch {
        viewModel.toBlock.collectLatest {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(it))
            startActivity(intent)
        }
    }

    private fun navigateOnHistory() = lifecycleScope.launch {
        viewModel.toHistory.collectLatest {
            TxHistoryBottomSheetDialog().show(supportFragmentManager, "TxHistoryBottomSheetDialog")
        }
    }

    private fun handleCommonErrors() = lifecycleScope.launch {
        lifecycle.repeatOnLifecycle(Lifecycle.State.RESUMED) {
            viewModel.onFailure.collectLatest {
                ErrorDialog().show(supportFragmentManager, "ErrorDialog")
            }
        }
    }
}
