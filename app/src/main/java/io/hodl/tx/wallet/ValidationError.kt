package io.hodl.presentation

import androidx.annotation.StringRes
import io.hodl.R
import java.math.BigDecimal

sealed class ValidationError(@StringRes val strRes: Int) {
    object AddressNotFilled : ValidationError(R.string.balance_activity_address_not_filled_error_text)
    object AmountNotFilled : ValidationError(R.string.balance_activity_amount_not_filled_error_text)
    object BalanceNotEnough : ValidationError(R.string.balance_activity_balance_not_enough_error_text)
    object FeeNotScored : ValidationError(R.string.balance_activity_fee_not_scored_text)
}


class FieldsValidator {

    fun validateAddress(value: String?): ValidationError? =
        if (value.isNullOrBlank()) ValidationError.AddressNotFilled else null

    fun validateSendAmount(
        value: BigDecimal?, availableBalance: BigDecimal?, minFee: BigDecimal?
    ): ValidationError? {
        value ?: return ValidationError.AmountNotFilled
        minFee ?: return ValidationError.FeeNotScored
        availableBalance ?: return ValidationError.BalanceNotEnough

        return if (availableBalance + minFee >= value)
            null
        else
            ValidationError.BalanceNotEnough
    }
}