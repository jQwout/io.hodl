package io.hodl.tx.wallet

import io.hodl.domain.TransactionHistoryRepository
import io.hodl.domain.WalletRepository
import io.hodl.domain.pojo.TransactionHistoryItem
import io.hodl.domain.pojo.WalletDto
import io.hodl.utils.TimeoutUtils.WALLET_POLLING_TIMEOUT
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*

class UpdateBalanceMiddleware(
    private val transactionHistoryRepository: TransactionHistoryRepository,
    private val walletRepository: WalletRepository
) {
    suspend fun loadData(): Flow<DataResult> {
        return flow {
            while (true) {
                val result = kotlin.runCatching {
                    val w = walletRepository.getWallet(true)
                    val h = transactionHistoryRepository.getTransactions(w.address)
                    w to h
                }
                emit(result)
                delay(WALLET_POLLING_TIMEOUT)
            }
        }
    }
}

typealias DataResult = Result<Pair<WalletDto, List<TransactionHistoryItem>>>