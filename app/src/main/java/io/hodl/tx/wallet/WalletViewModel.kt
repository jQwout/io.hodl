package io.hodl.tx.wallet

import android.util.Log
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider.AndroidViewModelFactory.Companion.APPLICATION_KEY
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import io.hodl.HodlApp
import io.hodl.domain.*
import io.hodl.domain.pojo.BitcoinTransaction
import io.hodl.domain.pojo.TransactionHistoryItem
import io.hodl.domain.pojo.WalletDto
import io.hodl.presentation.FieldsValidator
import io.hodl.presentation.ValidationError
import io.hodl.tx.fee.FeeState
import io.hodl.tx.fee.ScoreFeeMiddleware
import io.hodl.tx.info.BlockObserverUrlBuilder
import io.hodl.utils.StringViewUtils
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import java.math.BigDecimal

class WalletViewModel(
    private val transactionsUseCase: TransactionsUseCase,
    private val fieldsValidator: FieldsValidator,
    private val savedStateHandle: SavedStateHandle,
    private val scoreFeeMiddleware: ScoreFeeMiddleware,
    private val updateBalanceMiddleware: UpdateBalanceMiddleware,
    private val blockObserverUrlBuilder: BlockObserverUrlBuilder,
    private val backgroundDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {

    private var loadDataJob: Job? = null

    private val walletFlow: MutableStateFlow<WalletDto?> = MutableStateFlow(null)
    private val amountFlow: StateFlow<BigDecimal?> = savedStateHandle.getStateFlow(AMOUNT_DATA, null)
    private val addressToFlow: StateFlow<String?> = savedStateHandle.getStateFlow(ADDRESS_DATA, null)
    private val validation: MutableStateFlow<Set<ValidationError>> = MutableStateFlow(setOf())
    private val historyFlow: MutableStateFlow<List<TransactionHistoryItem>?> = MutableStateFlow(null)
    private val transactionFlow: StateFlow<BitcoinTransaction?> = savedStateHandle.getStateFlow(TRANSACTION_DATA, null)
    private val onTxSuccessEvent = Channel<String>()
    private val onFailureEvent = Channel<Throwable>()
    private val toHistoryEvent = Channel<Unit>()
    private val toBlockEvent = Channel<String>()

    val isDataLoading = MutableStateFlow(false)
    val feeFlow: StateFlow<FeeState> =
        savedStateHandle.getStateFlow(FEE_AMOUNT_DATA, FeeState.NotYet)
    val balance = walletFlow.mapNotNull {
        val amount = it?.balanceAvailable?.setScale(10) ?: return@mapNotNull null
        StringViewUtils.formatBtc(amount)
    }
    val walletAddress = walletFlow.mapNotNull { it?.address }
    val sendEnabled = feeFlow.map { it is FeeState.Scored }
    val amountValidationError = validation.map { set ->
        set.firstOrNull { it is ValidationError.AmountNotFilled || it is ValidationError.BalanceNotEnough }
    }
    val addressValidationError = validation.map { set ->
        set.filterIsInstance<ValidationError.AddressNotFilled>().firstOrNull()
    }
    val historyList = historyFlow.asStateFlow()
    val historyButtonEnable = historyFlow.map { it.isNullOrEmpty().not() }
    val onTxSuccess = onTxSuccessEvent.receiveAsFlow()
    val onFailure = onFailureEvent.receiveAsFlow()
    val toHistory = toHistoryEvent.receiveAsFlow()
    val toBlock = toBlockEvent.receiveAsFlow()

    init {
        loadDataWithRepeat()
        loadFeeOnInput()
    }

    fun inputAmount(value: CharSequence?) {
        validation.update {
            it.minusElement(ValidationError.AmountNotFilled)
                .minusElement(ValidationError.BalanceNotEnough)
        }
        savedStateHandle[AMOUNT_DATA] = value.toString().toBigDecimalOrNull()
    }

    fun inputAddress(value: CharSequence?) {
        validation.update {
            it.minusElement(ValidationError.AddressNotFilled)
                .minusElement(ValidationError.BalanceNotEnough)
        }
        savedStateHandle[ADDRESS_DATA] = value.toString()
    }

    fun onSendClicked() {
        viewModelScope.launch {
            preValidateOnSend()
            if (validation.value.isEmpty()) {
                isDataLoading.value = true
                try {
                    val txId = transactionsUseCase.sendRawTransaction(transactionFlow.value!!).getOrThrow()
                    onTxSuccessEvent.send(txId)
                } catch (e: Throwable) {
                    if (e !is CancellationException) {
                        onFailureEvent.send(e)
                    }
                    Log.d(this::class.java.simpleName, e.localizedMessage, e)
                } finally {
                    isDataLoading.value = false
                }
            }
        }
    }

    fun needReloadScreen() {
        viewModelScope.launch(backgroundDispatcher) {
            loadDataWithRepeat()
        }
    }

    fun onHistoryClicked() {
        viewModelScope.launch { toHistoryEvent.send(Unit) }
    }

    fun onTransactionClicked(txId: String) {
        viewModelScope.launch { toBlockEvent.send(blockObserverUrlBuilder.build(txId)) }
    }

    private fun loadDataWithRepeat() {
        isDataLoading.value = true
        loadDataJob?.cancel()
        loadDataJob = viewModelScope.launch(backgroundDispatcher) {
            updateBalanceMiddleware.loadData().collectLatest { result ->
                result
                    .onSuccess { (wallet, history) ->
                        walletFlow.value = wallet
                        historyFlow.value = history
                        isDataLoading.value = false
                    }
                    .onFailure {
                        Log.d(this::class.java.simpleName, it.localizedMessage, it)
                        if (walletFlow.value == null) {
                            onFailureEvent.send(it)
                        }
                        isDataLoading.value = false
                    }
            }
        }
    }

    private fun loadFeeOnInput() {
        viewModelScope.launch(backgroundDispatcher) {
            scoreFeeMiddleware.score(amountFlow, addressToFlow)
                .collectLatest { txAndFee ->
                    savedStateHandle[TRANSACTION_DATA] = txAndFee.first
                    savedStateHandle[FEE_AMOUNT_DATA] = txAndFee.second
                }
        }
    }

    private fun preValidateOnSend() {
        validation.value = buildSet {
            fieldsValidator.validateAddress(addressToFlow.value)?.let(::add)
            fieldsValidator.validateSendAmount(
                amountFlow.value,
                walletFlow.value?.balanceAvailable,
                feeFlow.value.scoredValueOrNull()
            )?.let(::add)
        }
    }

    companion object {
        private const val ADDRESS_DATA = "ADDRESS_DATA"
        private const val AMOUNT_DATA = "AMOUNT_DATA"
        private const val FEE_AMOUNT_DATA = "FEE_AMOUNT_DATA"
        private const val TRANSACTION_DATA = "TRANSACTION_DATA"

        fun factory() = viewModelFactory {
            initializer {
                val component = (this[APPLICATION_KEY] as HodlApp).transactionComponent
                val handler = createSavedStateHandle()
                val scoreFeeMiddleware = ScoreFeeMiddleware(component.transactionsUseCase)
                val updateBalanceMiddleware =
                    UpdateBalanceMiddleware(component.transactionHistoryRepository, component.walletRepository)
                WalletViewModel(
                    component.transactionsUseCase,
                    FieldsValidator(),
                    handler,
                    scoreFeeMiddleware,
                    updateBalanceMiddleware,
                    BlockObserverUrlBuilder.Impl(),
                )
            }
        }
    }
}