package io.hodl.tx.history

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.hodl.databinding.TxHistoryFragmentBinding
import io.hodl.tx.wallet.WalletViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch

class TxHistoryBottomSheetDialog : BottomSheetDialogFragment() {

    private val viewModel: WalletViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, theme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = TxHistoryFragmentBinding.inflate(inflater, container, false).root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = TxHistoryAdapter { viewModel.onTransactionClicked(it.txId) }.apply {
            TxHistoryFragmentBinding.bind(view).list.adapter = this
        }
        updateList(adapter)
    }

    private fun updateList(adapter: TxHistoryAdapter) = lifecycleScope.launch {
        viewModel.historyList.filterNotNull().collectLatest { adapter.submitList(it) }
    }
}