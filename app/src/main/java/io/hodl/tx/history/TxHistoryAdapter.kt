package io.hodl.tx.history

import android.annotation.SuppressLint
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import io.hodl.R
import io.hodl.databinding.TxHistoryItemBinding
import io.hodl.domain.pojo.TransactionHistoryItem
import io.hodl.tx.info.TxInfoSpannableFactory
import io.hodl.utils.StringViewUtils
import org.bitcoinj.core.Coin

class TxHistoryAdapter(
    val onItemClick : (TransactionHistoryItem) -> Unit
) : RecyclerView.Adapter<TxHistoryAdapter.ViewHolder>() {

    private val listDiffer = AsyncListDiffer(this, Differ)

    fun submitList(list: List<TransactionHistoryItem>) {
        listDiffer.submitList(list)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TxHistoryAdapter.ViewHolder {
        return ViewHolder(
            TxHistoryItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TxHistoryAdapter.ViewHolder, position: Int) {
        val item = listDiffer.currentList.get(position)
        holder.bind(item)
        holder.binding.root.setOnClickListener {
            onItemClick(item)
        }
    }

    override fun getItemCount(): Int {
        return listDiffer.currentList.size
    }

    class ViewHolder(
        val binding: TxHistoryItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        private val ctx = binding.root.context

        @SuppressLint("SetTextI18n")
        fun bind(transactionHistoryItem: TransactionHistoryItem) {
            val symbol = if (transactionHistoryItem.isSend) {
                ctx.getString(R.string.tx_history_item_sign_send)
            } else {
                ctx.getString(R.string.tx_history_item_sign_receive)
            }
            val color = if (transactionHistoryItem.isSend) {
                ContextCompat.getColor(ctx, R.color.gray)
            } else {
                ContextCompat.getColor(ctx, R.color.green)
            }
            binding.time.text = transactionHistoryItem.time?.let { StringViewUtils.formatDate(it) }
                ?: ctx.getString(R.string.tx_history_item_unconfirmed)
            binding.amount.text = "$symbol ${
                StringViewUtils.formatBtc(transactionHistoryItem.amount)
            }"
            binding.amount.setTextColor(color)
            setSpannable(StringViewUtils.txStrHumanReadable(transactionHistoryItem.txId))
        }

        private fun setSpannable(txStr: String) {
            binding.from.movementMethod = LinkMovementMethod.getInstance()
            binding.from.text = TxInfoSpannableFactory.spannable(
                ctx.getString(R.string.tx_history_from_prefix),
                txStr,
                ContextCompat.getColor(ctx, R.color.purple_500)
            ) {
                binding.root.callOnClick()
            }
        }
    }

    object Differ : DiffUtil.ItemCallback<TransactionHistoryItem>() {
        override fun areItemsTheSame(
            oldItem: TransactionHistoryItem,
            newItem: TransactionHistoryItem
        ): Boolean {
            return oldItem.txId == newItem.txId
        }

        override fun areContentsTheSame(
            oldItem: TransactionHistoryItem,
            newItem: TransactionHistoryItem
        ): Boolean {
            return oldItem == newItem
        }
    }
}