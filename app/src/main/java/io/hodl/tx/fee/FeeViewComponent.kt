package io.hodl.tx.fee

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import io.hodl.R
import io.hodl.databinding.ComissionViewComponentBinding
import io.hodl.tx.fee.FeeState
import io.hodl.utils.StringViewUtils.formatBtc

class FeeViewComponent(
    private val binding: ComissionViewComponentBinding,
) {

    private val ctx: Context = binding.root.context

    fun bind(feeState: FeeState): Unit = with(binding) {
        root.isVisible = feeState !is FeeState.NotYet
        feeViewProgress.isVisible = feeState is FeeState.InProgress

        when (feeState) {
            is FeeState.Error -> {
                feeViewText.setText(R.string.fee_score_has_error)
                feeViewText.setTextColor(
                    ContextCompat.getColor(ctx, R.color.red)
                )
            }
            is FeeState.InProgress -> {
                feeViewText.setText(R.string.fee_score_in_progress)
                feeViewText.setTextColor(
                    ContextCompat.getColor(ctx, R.color.black)
                )
            }
            is FeeState.NotYet -> Unit
            is FeeState.Scored -> {
                feeViewText.setText(
                    ctx.getString(R.string.fee_score_amount_template, formatBtc(feeState.amount))
                )
                feeViewText.setTextColor(
                    ContextCompat.getColor(ctx, R.color.black)
                )
            }
        }
    }
}