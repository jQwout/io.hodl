package io.hodl.tx.fee

import java.math.BigDecimal

sealed class FeeState : java.io.Serializable {
    object NotYet : FeeState()
    object InProgress : FeeState()
    class Scored(val amount: BigDecimal) : FeeState()
    class Error(val throwable: Throwable) : FeeState()

    fun scoredValueOrNull() = (this as? Scored)?.amount
}