package io.hodl.tx.fee

import io.hodl.domain.TransactionsUseCase
import io.hodl.domain.pojo.BitcoinTransaction
import io.hodl.tx.fee.FeeState
import io.hodl.utils.TimeoutUtils
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import java.math.BigDecimal

class ScoreFeeMiddleware(
    private val transactionsUseCase: TransactionsUseCase
) {

    suspend fun score(
        amountFlow: Flow<BigDecimal?>,
        addressToFlow: Flow<String?>
    ): Flow<TransactionAndFee> = combine(amountFlow, addressToFlow) { amount, address ->
        object {
            val addressTo = address
            val amount = amount
        }
    }
        .transform {
            try {
                if (it.addressTo.isNullOrBlank() || (it.amount == null || it.amount == BigDecimal.ZERO)) {
                    emit(null to FeeState.NotYet)
                } else {
                    emit(null to FeeState.InProgress)
                    delay(TimeoutUtils.NON_FLASH_TIMEOUT)
                    val tx = transactionsUseCase.buildTransactionFromUtxos(it.addressTo, it.amount)
                    emit(tx to FeeState.Scored(tx.fee))
                }
            } catch (e: Throwable) {
                emit(null to FeeState.Error(e))
            }
        }
}


typealias TransactionAndFee = Pair<BitcoinTransaction?, FeeState>