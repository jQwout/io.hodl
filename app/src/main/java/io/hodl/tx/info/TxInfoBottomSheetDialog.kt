package io.hodl.tx.info

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.hodl.R
import io.hodl.databinding.TxInfoBinding
import io.hodl.tx.wallet.WalletActivity
import io.hodl.tx.wallet.WalletViewModel
import io.hodl.utils.OnErrorDialogCancel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class TxInfoBottomSheetDialog : BottomSheetDialogFragment() {

    private val viewModel: WalletViewModel by viewModels {
        WalletViewModel.factory()
    }

    private val txId by lazy(LazyThreadSafetyMode.NONE) { requireArguments().getString(TX_ID_DATA)!! }
    private val txString by lazy(LazyThreadSafetyMode.NONE) { requireArguments().getString(TX_STR_DATA)!! }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, theme)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = TxInfoBinding.inflate(inflater, container, false).root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(TxInfoBinding.bind(view)) {
            txInfoText.movementMethod = LinkMovementMethod.getInstance()
            txInfoText.text = TxInfoSpannableFactory.spannable(
                getString(R.string.tx_info_tx_description),
                txString,
                ContextCompat.getColor(requireContext(), R.color.purple_500)
            ) {
                viewModel.onTransactionClicked(txId)
            }
            txInfoSendMoreButton.setOnClickListener {
                dismissNow()
            }
        }

        lifecycleScope.launch {
            viewModel.toBlock.collectLatest {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(it))
                startActivity(intent)
            }
        }

    }

    override fun onCancel(dialog: DialogInterface) {
        (activity as? OnErrorDialogCancel)?.onErrorDialogCancel()
        super.onCancel(dialog)
    }

    override fun onDismiss(dialog: DialogInterface) {
        (activity as? OnErrorDialogCancel)?.onErrorDialogCancel()
        super.onDismiss(dialog)
    }

    companion object {

        const val TX_ID_DATA = "txId"
        const val TX_STR_DATA = "txStr"
        fun create(
            txId: String,
            txStr: String
        ): TxInfoBottomSheetDialog = TxInfoBottomSheetDialog().apply {
            arguments = bundleOf(TX_ID_DATA to txId, TX_STR_DATA to txStr)
        }
    }
}


