package io.hodl.tx.info


interface BlockObserverUrlBuilder {

    fun build(txId: String) = path + txId

    class Impl : BlockObserverUrlBuilder

    companion object {
        const val path = "https://blockstream.info/testnet/tx/"
    }
}