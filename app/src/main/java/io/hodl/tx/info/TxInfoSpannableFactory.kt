package io.hodl.tx.info

import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View

object TxInfoSpannableFactory {

    fun spannable(description: String,
                  txStr: String,
                  txColor: Int,
                  onClick: () -> Unit = {}
    ): SpannableString {
        val full = "$description $txStr"
        val sp = object : ClickableSpan() {
            override fun onClick(widget: View) {
               onClick()
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.color = txColor
            }
        }
        return SpannableString(full).apply {
            setSpan(sp, description.length, full.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
    }
}